# FlyhtTwitterAggregator

This app reads first XXX tweets for one or more htags into file(s) and then monitors for updates. 

You need to provide your own 
twitter.consumer-key AND twitter.consumer-secret parameters  in [src/main/resources/twitter.properties] file in order to run this app.
