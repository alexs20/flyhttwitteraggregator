package com.wolandsoft.fta_test_1;

import java.io.IOException;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import com.wolandsoft.fta_test_1.core.FtaCore;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class App {
	@Autowired
	private FtaCore core;

	public static void main(String[] args) throws InterruptedException, BeansException, IOException {
		final ApplicationContext ctx = SpringApplication.run(App.class, args);
		ctx.getBean(FtaCore.class).initWithUserDate();
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				SpringApplication.exit(ctx);
				synchronized (ctx) {
					ctx.notify();
				}
			}
		});
		synchronized (ctx) {
			ctx.wait();
		}
	}

	@PostConstruct
	void onStart() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	@PreDestroy
	public void onShutdown() {
		core.shutdown();
	}
}