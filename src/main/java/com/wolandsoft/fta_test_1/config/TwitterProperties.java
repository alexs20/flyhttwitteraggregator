package com.wolandsoft.fta_test_1.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:twitter.properties")
@ConfigurationProperties(prefix = "twitter")
public class TwitterProperties {

	public String consumerKey;
	public String consumerSecret;
	public long readEveryMSeconds;
	public int firstReadTweets;

	public String getConsumerKey() {
		return consumerKey;
	}

	public void setConsumerKey(String consumerKey) {
		this.consumerKey = consumerKey;
	}

	public String getConsumerSecret() {
		return consumerSecret;
	}

	public void setConsumerSecret(String consumerSecret) {
		this.consumerSecret = consumerSecret;
	}

	public long getReadEveryMSeconds() {
		return readEveryMSeconds;
	}

	public void setReadEveryMSeconds(long readEveryMSeconds) {
		this.readEveryMSeconds = readEveryMSeconds;
	}

	public int getFirstReadTweets() {
		return firstReadTweets;
	}

	public void setFirstReadTweets(int firstReadTweets) {
		this.firstReadTweets = firstReadTweets;
	}

}
