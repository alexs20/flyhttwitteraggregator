package com.wolandsoft.fta_test_1.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

@Configuration
@EnableScheduling
public class SchedulerConfiguration implements SchedulingConfigurer {
	private static final int THREADS_SERVING = 1;

	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
		taskScheduler.setThreadPriority(Thread.NORM_PRIORITY - 1);
		taskScheduler.setPoolSize(THREADS_SERVING);
		taskScheduler.initialize();
		taskRegistrar.setTaskScheduler(taskScheduler);
	}

}
