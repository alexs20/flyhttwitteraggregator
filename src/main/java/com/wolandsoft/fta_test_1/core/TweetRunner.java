package com.wolandsoft.fta_test_1.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wolandsoft.fta_test_1.exception.RateException;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.RateLimitStatus;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

public class TweetRunner implements Runnable {
	private static Logger log = LoggerFactory.getLogger(TweetRunner.class);
	private long sinceId = 0;
	private int maxMessages;
	private Twitter twitter;
	private String topic;
	private File outputFile;

	public TweetRunner(Twitter twitter, String topic, int maxMessages) {
		super();
		this.twitter = twitter;
		this.topic = topic;
		this.maxMessages = maxMessages;
		File dir = new File(topic);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		outputFile = new File(dir, topic + ".txt");
	}

	@Override
	public void run() {
		Query query = new Query("#" + topic);
		query.setCount(maxMessages);
		if (sinceId != 0) {
			query.setSinceId(sinceId);
		}
		List<Status> statuses = null;
		do {
			try {
				QueryResult result = twitter.search(query);
				statuses = result.getTweets();
				if (statuses != null && !statuses.isEmpty()) {
					log.info("Dumping {} messages into [{}]", statuses.size(), outputFile.getAbsoluteFile());
					try (FileWriter fr = new FileWriter(outputFile, true);
							BufferedWriter br = new BufferedWriter(fr);
							PrintWriter pr = new PrintWriter(br)) {
						for (int i = statuses.size() - 1; i >= 0; i--) {
							Status status = statuses.get(i);
							sinceId = status.getId();
							pr.println("@" + status.getUser().getScreenName() + " : " + status.getUser().getName() + " at " + status.getCreatedAt());
							pr.println(status.getText());
						}
					}
				}
				query.setSinceId(sinceId);
			} catch (TwitterException te) {
				RateLimitStatus rts = te.getRateLimitStatus();
				if (rts.getRemaining() == 0) {
					log.warn("Rate limit have been exhausted (max:{}). Will reset in {} seconds.", rts.getLimit(), rts.getSecondsUntilReset() );
					throw new RateException();
				} else {
					throw new RuntimeException(te);
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		} while (statuses != null && !statuses.isEmpty());

	}

}
