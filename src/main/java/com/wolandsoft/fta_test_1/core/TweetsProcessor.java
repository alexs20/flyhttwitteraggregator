package com.wolandsoft.fta_test_1.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.wolandsoft.fta_test_1.config.TwitterProperties;
import com.wolandsoft.fta_test_1.exception.RateException;

import twitter4j.Twitter;

@Service
public class TweetsProcessor {

	@Autowired
	private TwitterProperties twitterProps;

	private Twitter twitter;
	private Set<String> topics;
	private List<TweetRunner> runners;
	private boolean isRunning = false;

	@PostConstruct
	void init() {
		topics = new HashSet<>();
		runners = new ArrayList<>();
	}

	public Twitter getTwitter() {
		return twitter;
	}

	public void setTwitter(Twitter twitter) {
		this.twitter = twitter;
	}

	public List<String> getTopics() {
		return Collections.unmodifiableList(new ArrayList<>(topics));
	}

	public void setTopic(String topic) {
		topics.add(topic);
	}

	public void start() {
		isRunning = true;
		for (String topic : topics) {
			TweetRunner runner = new TweetRunner(twitter, topic, twitterProps.getFirstReadTweets());
			runners.add(runner);
		}
	}

	public void shutdown() {

	}

	@Scheduled(fixedDelayString = "#{twitterProperties.readEveryMSeconds}", initialDelay = 10000)
	public void processInboundMessages() {
		if (isRunning) {
			try {
				for (TweetRunner runner : runners) {
					// better not to do that in multithreaded environment to not get banned by
					// trwitter.
					runner.run();
				}
			} catch (RateException re) {
				// nothing to do, just wait
			}
		}
	}

}
