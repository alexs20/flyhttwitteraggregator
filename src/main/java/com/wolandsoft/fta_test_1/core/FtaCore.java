package com.wolandsoft.fta_test_1.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import com.wolandsoft.fta_test_1.config.TwitterProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

@Service
public class FtaCore {
	private static Logger log = LoggerFactory.getLogger(FtaCore.class);
	@Autowired
	private TwitterProperties twitterProps;
	@Autowired
	private TweetsProcessor tweetsProcessor;

	private BufferedReader cli = new BufferedReader(new InputStreamReader(System.in));

	public void initWithUserDate() throws IOException {
		initAccountAccess();
		initTargetTasks();
		tweetsProcessor.start();
	}

	private void initAccountAccess() throws IOException {
		Twitter twitter = new TwitterFactory().getInstance();
		twitter.setOAuthConsumer(twitterProps.getConsumerKey(), twitterProps.getConsumerSecret());
		RequestToken requestToken = null;
		AccessToken accessToken = null;
		try {
			requestToken = twitter.getOAuthRequestToken();
		} catch (Exception e) {
			log.error("Error initializing twitter library.", e);
			throw new RuntimeException(e);
		}

		System.out.println("This application is using YOUR Twitter account to do all that stuff you asked for, therefore ...");
		while (null == accessToken) {
			System.out.println("Open the following URL in web browser and grant access to your account.");
			System.out.println(requestToken.getAuthorizationURL());
			System.out.print("Then grab the PIN and enter it here. [PIN]:");
			String pin = cli.readLine();
			try {
				if (pin.length() > 0) {
					accessToken = twitter.getOAuthAccessToken(requestToken, pin);
					twitter.setOAuthAccessToken(accessToken);
				}
			} catch (TwitterException e) {
				log.error("Error retrieving OAuth token.", e);
			}
		}

		System.out.println("Great, we got the access token: " + accessToken.getToken());
		tweetsProcessor.setTwitter(twitter);
	}

	private void initTargetTasks() throws IOException {
		System.out.println(
				"Now, give me topic names to fetch from Twitter. You can either type them all, comma separated, or one by one ...");
		String lastInput = null;
		while (lastInput == null || lastInput.length() > 0) {
			System.out.print("Topic name or empty line to finish. " + tweetsProcessor.getTopics().toString() + " +:");
			lastInput = cli.readLine();
			if (lastInput.length() > 0) {
				lastInput = lastInput.trim();
				if (lastInput.length() > 0) {
					for (String topic : lastInput.split(",")) {
						tweetsProcessor.setTopic(topic.trim());
					}
				}
			} else if (tweetsProcessor.getTopics().size() == 0) {
				System.out.println("You need to supply at least one topic");
				lastInput = null;
			} else {
				System.out.println("Wonderful, now we can grab some data. Please note that refresh time is " + twitterProps.getReadEveryMSeconds() / 1000 + " seconds");
			}
		}
	}

	public void shutdown() {
		tweetsProcessor.shutdown();
	}
}
